let dale = [...document.body.querySelectorAll('input[type=checkbox]')]

function atualizarEstado() {
    let novasTags = []
    for(check of dale){
        let tag = check.id.split('-')[2]
        let xecado = check.checked
        if(xecado) novasTags.push(tag)
    }
    if(novasTags.length == 0){
        document.body.querySelectorAll('#card-projeto').forEach(elm => {
            elm.classList.remove('d-none')
        })
    } else {
        document.body.querySelectorAll('#card-projeto').forEach(elm => {
            let tags = [...elm.querySelectorAll('li')].map(e => e.textContent)
            let visivel = novasTags.every(tag => tags.includes(tag))
    
            if(visivel) elm.classList.remove('d-none')
            else        elm.classList.add('d-none')
        })
    }

}