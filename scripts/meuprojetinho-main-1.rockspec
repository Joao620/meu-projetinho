rockspec_format = "3.0"
package = "meuprojetinho"
version = "main-1"
source = {
   url = "git+https://github.com/Joao620/meuprojetinho.git"
}
description = {
   homepage = "https://github.com/Joao620/meuprojetinho",
   license = "*** please specify a license ***"
}
dependencies = {
   "luafilesystem",
   "lua-toml",
   "path",
   "htmlparser",
   "inspect",
}
