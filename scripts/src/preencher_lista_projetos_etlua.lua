local file = require('file')
local htmlparser = require "htmlparser"


local function contem(val, table)
   for i=1,#table do
      if table[i] == val then
         return true
      end
   end
   return false
end

local function preencher_lista(configs_dos_projetos, html)
  local root = htmlparser.parse(html)

  local card_template = root:select('#card-projeto')[1]
  local card_html = card_template:gettext()
  -- TODO: identar o card_html direto, que assim vai identar todos lá pra fente automatico

  local html_projetos = {}
  for _, configs in pairs(configs_dos_projetos) do
    if configs['imagem'] then
      configs['proj_image'] = configs['caminho'] .. '/' .. configs['imagem']
    else
      configs['proj_image'] = '../assets/img/media-image-xmark.svg'
    end
    
    local tag_template = card_template:select('#tag-projeto')[1]
    local tag_html = tag_template:gettext()

    local tags = {}
    for _, projeto_tags in pairs(configs.tags) do
      for _, tag_projeto in ipairs(projeto_tags) do
        local tag_sendo_gerada = tag_html:gsub('{{tag}}', tag_projeto)
        table.insert(tags, tag_sendo_gerada)
      end
    end

    local comeco_offsetado = tag_template._openstart - card_template._openstart
    local final_offsetado = tag_template._closeend - card_template._openstart + 2

    local card_com_tags = (
      card_html:sub(1, comeco_offsetado) ..
      table.concat(tags, '\n') ..
      card_html:sub(final_offsetado)
    )

    local projeto_sendo_gerado = card_com_tags:gsub('{{.-}}', function(match)
      local chave = match:sub(3, -3)
      return configs[chave]
    end)

    table.insert(html_projetos, projeto_sendo_gerado)
  end

  local projetos_concatenados = table.concat(html_projetos, '\n')

  local html_final = (
    html:sub(1, card_template._openstart - 1) ..
    projetos_concatenados ..
    html:sub(card_template._closeend + 1)
  )

  return html_final
end

local function preencher_filtro(configs_dos_projetos, html)
  local tags_deduplicadas = {}

  for _, configs in pairs(configs_dos_projetos) do
    for categoria, tags in pairs(configs.tags) do
      tags_deduplicadas[categoria] = tags_deduplicadas[categoria] or {}
      for _, tag in ipairs(tags) do
        if not contem(tag, tags_deduplicadas[categoria]) then
          table.insert(tags_deduplicadas[categoria], tag)
        end
      end
    end
  end

  local root = htmlparser.parse(html)
  local acccordion_template = root:select('#accordiones')[1]
  local accordion_html = acccordion_template:gettext()

  local checkbox_template = acccordion_template:select('#checkbox-filtro')[1]
  local checkbox_html = checkbox_template:gettext()

  local accordions = {}
  for categoria, itens in pairs(tags_deduplicadas) do
    local checkboxes = {}
    for _, tag in ipairs(itens) do
      local checkbox = checkbox_html:gsub('{{tag}}', tag)
      table.insert(checkboxes, checkbox)
    end

    local comeco_offsetado = checkbox_template._openstart - acccordion_template._openstart
    local final_offsetado = checkbox_template._closeend - acccordion_template._openstart + 2 -- não entendi porque +2, mas precisa dele

    local accordion = (
      accordion_html:sub(1, comeco_offsetado) ..
      table.concat(checkboxes, '\n') ..
      accordion_html:sub(final_offsetado)
    ):gsub('{{categoria}}', categoria)

    table.insert(accordions, accordion)
  end

  local html_final = (
    html:sub(1, acccordion_template._openstart - 1) ..
    table.concat(accordions, '\n') ..
    html:sub(acccordion_template._closeend + 1)
  )

  return html_final
end

return function(configs_dos_projetos, template_html, caminho_salvar)
  local html_string = file.read(template_html)

  html_string = preencher_lista(configs_dos_projetos, html_string)
  local html_preenchido = preencher_filtro(configs_dos_projetos, html_string)

  file.write(caminho_salvar, html_preenchido)
end
