lfs = require('lfs')


local function dirCerto2(path)
  iter = lfs.dir(path)

  return function()
    while true do
      local path_loop = iter()
      if path_loop ~= '.' and path_loop ~= '..' then return path_loop end
    end
  end
end

for projeto in dirCerto2(".") do
  print(projeto)
end
