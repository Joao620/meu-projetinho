local file = require('file')
local inspect = require('inspect')

local function contem(val, table)
   for i=1,#table do
      if table[i] == val then
         return true
      end
   end
   return false
end

local function preencher_lista(configs_dos_projetos, html)
  local sinalizador_final = '<!%-%-#card%-projeto final%-%->'
  local magic = '(<div[^><]-id="card%-projeto".-)'..sinalizador_final

  local card_template = string.match(html, magic)
  card_template = card_template:gsub('id="card%-projeto"', '')

  local lista_dos_html = {}
  for id, configs in pairs(configs_dos_projetos) do
    local caminho_imagem = id..'/'..configs.imagem
    local html_sendo_gerado = card_template
      :gsub('##titutlo##', configs.titulo, 1)
      :gsub('##descricao##', configs.descricao, 1)
      :gsub('(<img.-src=").-%.png(")', '%1'..caminho_imagem..'%2', 1)
      :gsub('(href=")#', '%1'..id..'/', 1)

    table.insert(lista_dos_html, html_sendo_gerado)
  end

  local html_final = table.concat(lista_dos_html, '\n')
  local lista_projetos_preenchida = html:gsub(magic, html_final)

  return lista_projetos_preenchida
end

local function preencher_filtro(configs_dos_projetos, html)
  local tags_deduplicadas = {}

  for _, configs in pairs(configs_dos_projetos) do
    for categoria, tags in pairs(configs.tags) do
      tags_deduplicadas[categoria] = tags_deduplicadas[categoria] or {}
      for _, tag in ipairs(tags) do
        if not contem(tag, tags_deduplicadas[categoria]) then
          table.insert(tags_deduplicadas[categoria], tag)
        end
      end
    end
  end

  local sinalizador_final = '<!%-%-accordion final%-%->'
  local magic_accordion = '(<div[^><]-id="accordiones".-)'..sinalizador_final
  local accordion_template = string.match(html, magic_accordion)
  print(magic_accordion)
  accordion_template = accordion_template:gsub('id="accordiones"', '')

  local magic_checkbox = '(<div[^><]-class="form%-check".-\n)'
  local checkbox_template = string.match(accordion_template, magic_checkbox)

  local accordions = {}
  for categoria, itens in pairs(tags_deduplicadas) do
    local html_sendo_gerado = accordion_template
      :gsub('categoria%-filtro', categoria)

      local html_itens = {}
      for _, item in ipairs(itens) do
        local item_sendo_gerado = checkbox_template
          :gsub('item%-nome', item)
          :gsub('item%-filtro', categoria..':'..item)
        table.insert(html_itens, item_sendo_gerado)
      end

      local accordion_final = html_sendo_gerado
        :gsub(magic_checkbox, table.concat(html_itens, '\n'))

      table.insert(accordions, accordion_final)
  end

  local html_final = html:gsub(magic_accordion, table.concat(accordions, '\n'))

  print(html_final)

  return html_final
end

return function(configs_dos_projetos, html_para_preencher, caminho_salvar)
  local html_string = file.read(html_para_preencher)

  html_string = preencher_lista(configs_dos_projetos, html_string)
  local html_preenchido = preencher_filtro(configs_dos_projetos, html_string)

  file.write(caminho_salvar, html_preenchido)
end
