inspect = require('inspect')

local M = {}
utils = M

function utils.ls(path)
  local caminhos_validos = {}
  for caminho in lfs.dir(path) do
    if caminho ~= '.' and caminho ~= '..' then
      table.insert(caminhos_validos, caminho)
    end
  end
  local index_caminhos = 0

  return function()
    index_caminhos = index_caminhos + 1
    return caminhos_validos[index_caminhos]
  end
end

---@alias parametro string
---@alias mensagem_caso_erro string

---@param tabela_args table<string, string> tabela do `cmdline.src.cmdline` com os parametros
---@param parametros_a_testar table<parametro, mensagem_caso_erro>
function utils.testar_args(tabela_args, parametros_a_testar)
  for arg, msg_erro in pairs(parametros_a_testar) do
    if not tabela_args[arg] then
      mensagem_erro = string.format("Argumento --%s não passado: %s", arg, msg_erro)
      error(mensagem_erro)
    end
  end

  return tabela_args
end

function utils.converter_toml(tabela)
  local toml = {}
  for chave, valor in pairs(tabela) do
    if not chave:find('%.') then
      toml[chave] = valor
    end
    
    local chave_split = {}
    for chave_part in chave:gmatch("[^%.]+") do
      table.insert(chave_split, chave_part)
    end

    for i = 1, #chave_split - 1 do
      local chave_atual = chave_split[i]
      local chave_proxima = chave_split[i + 1]
      if not toml[chave_atual] then
        toml[chave_atual] = {}
      end
      if i == #chave_split - 1 then
        toml[chave_atual][chave_proxima] = valor
      end
    end
  end
  
  return toml

end

return M
