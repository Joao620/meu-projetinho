lfs = require('lfs')
path = require('path')
utils = require('utils')
file = require('file')
args = require('cmdline')

local raiz = args.parse(arg).raiz or './'

local parametros = {
  lista_projetos = 'dist/lista-projetos/index-template.html',
  template_projeto = 'dist/lista-projetos/projeto/projeto-template.html',
  pasta_projetos = 'projetos/',
  exportar_projetos = 'dist/lista-projetos/',
}

for chave, param in pairs(parametros) do
  parametros[chave] = path.exists(raiz..param)
end

htmlTemplate = assert(file.read(parametros.template_projeto), 'arquivo de template parece não existir')

package.path = lfs.currentdir()..'/?.lua;'..package.path
assert(lfs.chdir(parametros.pasta_projetos), 'não foi possível navegar para a pasta de projetos')
path_projetos = lfs.currentdir()

local configs_dos_projetos = require('renderizar_projetos')(
  path_projetos,
  parametros.exportar_projetos,
  htmlTemplate
)

-- local caminho_configs = path.normalize(parametros.exportar_projetos, 'projetos.json')
-- local configs_json = toml.toJSON(configs_dos_projetos)  acabei não usando esses bbaguis
-- file.write(caminho_configs, configs_json, 'w')

local lista_projetos_output = path.normalize(path.dirname(parametros.lista_projetos), 'index.html')
require('preencher_lista_projetos_etlua')
  (configs_dos_projetos, parametros.lista_projetos, lista_projetos_output)
