local md = require('luamd')
local lfs = require('lfs')
local path = require('path')
local file = require('file')
local toml = require('toml')
local inspect = require("inspect")

local function get_config(projeto_absoluto)
  projeto_toml = file.read('config.toml')
  if not projeto_toml then
    error('arquivo config.toml do projeto `' .. projeto .. '` nao existe')
  end

  --local succeeded, config_projeto = pcall(toml.decode, projeto_toml)
  config_projeto = toml.parse(projeto_toml)
  if not succeeded then
    error_string = string.format('erro em parsear a config do projeto `%s`: \n%s', projeto, inspect(config_projeto))
    error(error_string)
  end

  return config_projeto
end

local function renderizar(caminho_projetos, caminhos_exportacao, template_html)
  local configs_dos_projetos = {}

  for projeto in utils.ls(caminho_projetos) do
    assert(file.read('index.md'), 
      'arquivo index.md do projeto `' .. projeto .. '` nao existe')

    config_projeto = get_config(projeto)

    htmlDoMarkdown = assert(md.renderString(projeto_markdown))
    htmlGerado = template_html
      :gsub('##markdown##', htmlDoMarkdown)
      :gsub('##titulo##', config_projeto.titulo)
      :gsub('##descricao##', config_projeto.descricao)

    caminho_projeto_exportado = path.normalize(caminhos_exportacao, projeto)
    if not path.exists(caminho_projeto_exportado) then
      lfs.mkdir(caminho_projeto_exportado)
    end

    caminho_html_projeto = path.normalize(caminho_projeto_exportado, 'index.html')
    file.write(caminho_html_projeto, htmlGerado)

    for arquivo in utils.ls('.') do
      if arquivo ~= 'index.md' and arquivo ~= 'config.toml' then
        file.copy(arquivo, path.normalize(caminhos_exportacao, projeto, arquivo))
      end
    end

    configs_dos_projetos[projeto] = config_projeto
  end

  return configs_dos_projetos
end

return renderizar
