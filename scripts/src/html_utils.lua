local htmlparser = require "htmlparser"

local M = {}

function M.selecionar_tag(html, id, incluir_espacos)
  local root = htmlparser.parse(htmlstring)

  local tag = root:select('#'..id)
  if not tag then return nil end

  return tag::gettext()
end

return M
