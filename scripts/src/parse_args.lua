local args = require('cmdline.src.cmdline')
local path = require('path')

local argumentos_pedidos = {
  template = 'arquivo que possui o template da página de projeto',
  projetos = 'pasta contendo a lista com o md dos projetos',
  exportar = 'pastas que vão ser colocado os projetos exportados'
}
local argumentos_passados = args.parse(arg)

-- testa se todos argumentos foram passados certinho
for arg, msg_erro in pairs(argumentos_pedidos) do
  if not argumentos_passados[arg] then
    mensagem_erro = string.format("Argumento --%s não passado: %s", arg, msg_erro)
    error(mensagem_erro)
  end
end

-- modifica todos caminhos passados para absoluto
for key, value in pairs(argumentos_passados) do
	argumentos_passados[key] = path.exists(value)
end

return argumentos_passados