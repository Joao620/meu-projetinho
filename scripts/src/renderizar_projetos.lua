local md = require('luamd')
local lfs = require('lfs')
local path = require('path')
local file = require('file')
local toml = require('toml')
local utils = require('utils')
local inspect = require("inspect")

local function renderizar(caminho_projetos, caminhos_exportacao, template_html)
  local configs_dos_projetos = {}

  for projeto in utils.ls(caminho_projetos) do
    print('Projeto:', projeto)
    projeto_absoluto = path.normalize(caminho_projetos, projeto)
    assert(lfs.chdir(projeto_absoluto))

    local projeto_markdown = assert(file.read("index.md"))
    local toml_texto = assert(file.read("config.toml"))

    local config_projeto_ruim = toml.parse(toml_texto)
    local config_projeto = utils.converter_toml(config_projeto_ruim)
    config_projeto['caminho'] = projeto

    html_do_markdown = assert(md.renderString(projeto_markdown))
    html_gerado = template_html:gsub('{{.-}}', function (match)
      return config_projeto[match:sub(3, -3)] or html_do_markdown
    end)

    caminho_projeto_exportado = path.normalize(caminhos_exportacao, projeto)
    if not path.exists(caminho_projeto_exportado) then
      lfs.mkdir(caminho_projeto_exportado)
    end

    caminho_html_projeto = path.normalize(caminho_projeto_exportado, 'index.html')
    file.write(caminho_html_projeto, html_gerado)

    for arquivo in utils.ls('.') do
      if arquivo ~= 'index.md' and arquivo ~= 'config.toml' then
        file.copy(arquivo, path.normalize(caminhos_exportacao, projeto, arquivo))
      end
    end

    configs_dos_projetos[projeto] = config_projeto
  end

  return configs_dos_projetos
end

return renderizar
