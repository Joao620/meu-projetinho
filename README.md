# Meu Projetinho

Um lugazinho para expor seus projetos. Vai ter muita mais informações no site mesmo, então visita lá:
https://joao620.gitlab.io/meu-projetinho

## Como testar localmente?

vai precisar de lua e luarocks, e então rodar algo nesse nível
```sh
cd scripts
luarocks make --only-deps --local meuprojetinho-main-1.rockspec
eval $(luarocks path --bin)
cd src
lua criar_html_projetos.lua --raiz=../../
```

sobre o eval ali, é para exportar o caminho das dependencias que foram instaladas localmente (na home do seu usuário)

depois usa algo como `python -m http.server` para os arquivos em `dist/`

## Bagunça

Isso daqui é um full MVP. O código tá bem bagunçado, tem várias coisinhas bobas que foram removidas, e outra que eu quero colocar. Mas ele já levou muito tempo para ser lancado (O arquivo mais antigo é de 2023-10-12!)
Mesmo assim, tô super feliz de ter conseguido lançar, e bem satisfeito com o resultado final. Agora só falta escrever uns posts em uns blogs, e tá feito!