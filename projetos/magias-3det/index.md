# Ideia

A ideia do site já é bem antiga, tanto que a lista de magias dele foi criado por volta de 09/2021. Então eu aproveitei ela junto com minha vontade de estudar Angular para fazer esse site.

Ela junta as magias dos livros "Manual 3D&T Alpha — Edição Revisada" e o "Manual da Magia" que é complementar, e uma para que quem precisar um `scraping` de um PDF, não copie o texto e tente tratar depois, use alguma ferramenta para transformar o pdf em XML ou em outro arquivo de marcação, não me pergunte como eu sei disso ;)

## Tecnologias

Foi feito com angular 14, está usando [lunr](https://lunrjs.com/) para fazer a pesquisa de texto, e [Skeleton](http://getskeleton.com/) para eu não precisar me preocupar muito com o css

## Resultado

Em geral, foi uma ótima primeira experiência com o angular, e agora eu posso dormir tranquilo com menos um projeto na minha lista de afazeres

sinta-se livre para experimentar em 
http://magias3det.hagaka.tech
